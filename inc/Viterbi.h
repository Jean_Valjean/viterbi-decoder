#pragma once

#include "systemc.h"
#include <tuple>
#include <list> 
#include <iterator> 
#include <algorithm>
#include "States.h"
#include "Path.h"
#include "PathSelector.h"

#ifdef Viterbi

using namespace std;

typedef struct {
	Path path;
	list<Path>::iterator pathIt;
} PathTrellis;

SC_MODULE(Viterbi)
{
	sc_in<bool> inA, inB;
	sc_in<uint8_t> input;
	sc_in<bool> clk;

	LookUpTable* lookUpTable;
	PathSelector* pathSelect;
	list<Path> pathsTrellis;

	//sc_in< list<Path>> test;

	uint8_t states[8] = {0b000, 0b001, 0b010, 0b011, 0b100, 0b101, 0b110, 0b111};

	uint8_t currentState = 0b000;
	Node currentNode = { 0b000, 0b000, 0, 0 };
	
	uint8_t currentInput = 0;

	list<LookUpElement> checkNextState(uint8_t currentState);

	uint8_t calculateMetric(LookUpElement lkUpEl);

	void createTrellis();

	list<PathTrellis> findPathsWithSameState(uint8_t state);
	
	uint8_t findLowestMetric(list<PathTrellis> paths);

	list<PathTrellis> findPathsLowestMetric(list<PathTrellis> paths);

	uint8_t findHighestMetric(list<Path> paths);

	Path findPathWithHighestMetric(list<Path> paths);

	void deletePathsFromTrellis(list<PathTrellis> paths2Delete);

	void deletePaths();

	void do_something();

	SC_CTOR(Viterbi)
	{
		pathSelect = new PathSelector("PathSelection");
		lookUpTable = new LookUpTable();
		SC_METHOD(do_something);
		sensitive << clk << input; //<< inA << inB
		dont_initialize();

	}

};

#endif