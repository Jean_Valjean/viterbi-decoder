#pragma once
#include "systemc.h"


struct Node {
	uint8_t state;
	uint8_t prevState;
	uint8_t metric;
	uint8_t codeword;
	uint8_t input_bit;

} ;