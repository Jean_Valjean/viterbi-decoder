#pragma once
#include <systemc.h>
#include <list>
#include "Path.h"
#include "bus_if_path_trellis.h"
#include "bus_trellis.h"

using namespace std;

SC_MODULE(Traceback)
{
	sc_port<bus_read_if_path_trellis> pathsIn;
	sc_in<bool> trigger;

	sc_out<uint8_t> decoderOutput;
	sc_out<bool> triggerMonitor;

	Path findPathWithHighestMetric(list<Path> paths);
	uint8_t findHighestMetric(list<Path> paths);

	void findPath();

	SC_CTOR(Traceback)
	{
		SC_METHOD(findPath);
		sensitive << trigger;
		dont_initialize();
	}
};