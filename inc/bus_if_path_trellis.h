#pragma once
#include <systemc.h>
#include "Path.h"
#include <list>

using namespace std;

class bus_write_if_path_trellis : virtual public sc_interface
{
public:
	virtual bool nb_write(list<Path>) = 0;
};

class bus_read_if_path_trellis : virtual public sc_interface
{
public:
	virtual bool nb_read(list<Path>&) = 0;
};