#pragma once
#include <systemc.h>

using namespace std;

SC_MODULE(Monitor)
{
	sc_in<bool> trigg;
	sc_in<uint8_t> decoderOut;

	void bin(unsigned n)
	{
		if (n > 1)
			bin(n >> 1);		
		cout << (n & 1);
	}

	void printResult()
	{		
		uint8_t decodedOutput = decoderOut.read();
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << "----------------- Decoded Output -----------------" << endl;
		bin((unsigned)decodedOutput);
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
	}

	SC_CTOR(Monitor)
	{
		SC_METHOD(printResult);
		sensitive << trigg;
		dont_initialize();
	}
};