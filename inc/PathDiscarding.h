#pragma once
#include <systemc.h>
#include <list>
#include "Path.h"
#include "bus_if_path_trellis.h"
#include "bus_trellis.h"

using namespace std;

typedef struct {
	Path path;
	list<Path>::iterator pathIt;
} PathTrellis;


SC_MODULE(PathDiscarder)
{

	sc_port<bus_read_if_path_trellis> trellisIn;
	sc_port<bus_write_if_path_trellis> trellisOut;

	sc_in<bool> clk;
	sc_out<bool> out2Traceback;
	
	list<Path> trellisPaths2Discard;

	uint8_t states[8] = { 0b000, 0b001, 0b010, 0b011, 0b100, 0b101, 0b110, 0b111 };

	list<PathTrellis> findPathsWithSameState(uint8_t state);
	uint8_t findLowestMetric(list<PathTrellis> paths);
	list<PathTrellis> findPathsLowestMetric(list<PathTrellis> paths);
	uint8_t findHighestMetric(list<Path> paths);
	Path findPathWithHighestMetric(list<Path> paths);
	void deletePathsFromTrellis(list<PathTrellis> paths2Delete);
	void PathsElimination();
	void deletePaths();

	SC_CTOR(PathDiscarder)
	{		
		SC_THREAD(deletePaths);
		sensitive << clk.pos();
		dont_initialize();
	}
};