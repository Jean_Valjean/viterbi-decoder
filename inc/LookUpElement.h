#pragma once
#include "systemc.h"
#include <tuple>
#include <list> 
#include <iterator> 
#include <ios>

struct LookUpElement
{
	uint8_t input_bit;
	uint8_t input_state;
	uint8_t output_state;
	uint8_t output_bit;

	LookUpElement& operator= (const LookUpElement& rhs)
	{
		input_bit = rhs.input_bit;
		input_state = rhs.input_state;
		output_state = rhs.output_state;
		output_bit = rhs.output_bit;
		return *this;
	}

	bool operator== (const LookUpElement& rhs) const
	{
		return (input_bit == rhs.input_bit && input_state == rhs.input_state && output_state == rhs.output_state
			&& output_bit == rhs.output_bit);
	}

	friend std::ostream& operator << (std::ostream & os, const LookUpElement& I); //< Declaration for the friend method.

	friend void sc_trace(sc_trace_file*& tf, const LookUpElement& trans, std::string nm);
};
