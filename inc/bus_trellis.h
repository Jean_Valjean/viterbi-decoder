#pragma once
#include <systemc.h>
#include "bus_if_path_trellis.h"
#include <systemc.h>
#include <tuple>
#include <list> 
#include <iterator> 
#include <algorithm>
#include "States.h"
#include "Path.h"

using namespace std;

class bus_trellis : public sc_module,
	public bus_read_if_path_trellis,
	public bus_write_if_path_trellis
{
private:
	list<Path> pathsTrellis;
public:
	bus_trellis(sc_module_name nm) : sc_module(nm)
	{

	}
	bool nb_write(list<Path>);
	bool nb_read(list<Path>&);
	void register_port(sc_port_base& port_, const char* if_typename_);
};