#pragma once
#include "systemc.h"
#include <tuple>
#include <list> 
#include <iterator> 
#include "LookUpElement.h"


class LookUpTable
{
public:

	std::list<LookUpElement> lookUpTable =
	{
		{0, 0b000, 0b000, 0b00},
		{1, 0b000, 0b100, 0b11},
		{0, 0b001, 0b000, 0b11},
		{1, 0b001, 0b100, 0b00},
		{0, 0b010, 0b001, 0b10},
		{1, 0b010, 0b101, 0b01},
		{0, 0b011, 0b001, 0b01},
		{1, 0b011, 0b101, 0b10},
		{0, 0b100, 0b010, 0b11},
		{1, 0b100, 0b110, 0b00},
		{0, 0b101, 0b010, 0b00},
		{1, 0b101, 0b110, 0b11},
		{0, 0b110, 0b011, 0b01},
		{1, 0b110, 0b111, 0b10},
		{0, 0b111, 0b011, 0b10},
		{1, 0b111, 0b111, 0b01}
	};

	
	LookUpTable()
	{

	}
};
