#pragma once
#include "systemc.h"
#include "Viterbi.h"
#include "Trellis.h"
#include "bus_trellis.h"

SC_MODULE(InputShifter)
{
	sc_out<uint8_t> output2Trellis{"out input shift to trellis"};
	sc_in<bool> clk{ "Input Clock Input Shifter" };
	sc_out<bool> trellisClk{"Output clock to trellis"};

	sc_uint<16> in;
	sc_uint<16> data_in = 0b01110111010111;
	//sc_uint<16> data_in = 0b01110111010111;

		

	void shift_input();
	void bin(unsigned n);

	SC_CTOR(InputShifter)
	{
		SC_CTHREAD(shift_input, clk.pos());
	}
};
