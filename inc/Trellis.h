#pragma once
#include <systemc.h>
#include <tuple>
#include <list> 
#include <iterator> 
#include <algorithm>
#include "States.h"
#include "Path.h"
#include "PathDiscarding.h"
#include "bus_if_path_trellis.h"
#include "LookUpElement.h"

using namespace std;

SC_MODULE(Trellis)
{
	//sc_out<bool> out2discarder;
	sc_port<bus_write_if_path_trellis> trellisOut;
	sc_port<bus_read_if_path_trellis> trellisIn;

	sc_in<uint8_t> input;
	sc_in<bool> clk;
	
	bool discardSign = false;

	LookUpTable* lookUpTable;

	list<Path> pathsTrellis;

	uint8_t currentState = 0b000;
	Node currentNode = { 0b000, 0b000, 0, 0 };

	uint8_t currentInput = 0;

	void createTrellis();

	list<LookUpElement> checkNextState(uint8_t currentState);
	uint8_t calculateMetric(LookUpElement lkUpEl);

	SC_CTOR(Trellis)
	{
		lookUpTable = new LookUpTable();

		//pathDiscarding->nodeTrellis(nodePathTrellis);

		SC_METHOD(createTrellis);
		sensitive << clk;
		dont_initialize();
	}
};