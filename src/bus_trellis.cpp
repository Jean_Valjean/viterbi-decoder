#include "bus_trellis.h"

bool bus_trellis::nb_write(list<Path> pathTrellis)
{
	pathsTrellis = pathTrellis;
	return true;
}

bool bus_trellis::nb_read(list<Path>& pathTrellis)
{
	if (pathsTrellis.size() != 0)
	{
		pathTrellis = pathsTrellis;
		return true;
	}
	return false;
	
}

void bus_trellis::register_port(sc_port_base& port_,
	const char* if_typename_)
{
	cout << "binding    " << port_.name() << " to "
		<< "interface: " << if_typename_ << endl;
}