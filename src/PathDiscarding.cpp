#include "PathDiscarding.h"

list<PathTrellis> PathDiscarder::findPathsWithSameState(uint8_t state)
{
	list<PathTrellis> pathsRepeated;

	for (list<Path>::iterator path = trellisPaths2Discard.begin();
		path != trellisPaths2Discard.end(); ++path)
	{
		if (state == path->nodes.front().state)
		{
			PathTrellis pathTrellis = { *path, path };
			pathsRepeated.push_front(pathTrellis);
		}
	}

	return pathsRepeated;
}

uint8_t PathDiscarder::findLowestMetric(list<PathTrellis> paths)
{
	list<uint8_t> metricList;

	for (list<PathTrellis>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		metricList.push_front(path->path.nodes.front().metric);
	}

	auto ite = min_element(metricList.begin(), metricList.end());

	uint8_t minMetric = (uint8_t)* ite;

	return minMetric;
}

list<PathTrellis> PathDiscarder::findPathsLowestMetric(list<PathTrellis> paths)
{
	list<PathTrellis> pathsLowestMetric;

	uint8_t lowestMetric = 0;

	lowestMetric = findLowestMetric(paths);

	for (list<PathTrellis>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		if (path->path.nodes.front().metric == lowestMetric)
		{
			pathsLowestMetric.push_front(*path);
		}
	}
	return pathsLowestMetric;
}

uint8_t PathDiscarder::findHighestMetric(list<Path> paths)
{
	list<uint8_t> metricList;

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		metricList.push_front(path->nodes.front().metric);
	}

	auto ite = max_element(metricList.begin(), metricList.end());

	uint8_t maxMetric = (uint8_t)* ite;

	return maxMetric;
}

Path PathDiscarder::findPathWithHighestMetric(list<Path> paths)
{
	Path pathHighestMetric;

	uint8_t highestMetric = 0;

	highestMetric = findHighestMetric(paths);

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		if (path->nodes.front().metric == highestMetric)
			pathHighestMetric = *path;
	}

	return pathHighestMetric;
}

void PathDiscarder::deletePathsFromTrellis(list<PathTrellis> paths2Delete)
{
	for (list<PathTrellis>::iterator path = paths2Delete.begin();
		path != paths2Delete.end(); ++path)
	{
		trellisPaths2Discard.erase(path->pathIt);
	}
}

void PathDiscarder::PathsElimination()
{
	for (uint8_t i = 0; i < 8; i++)
	{
		list<PathTrellis> pathWithState = findPathsWithSameState(states[i]);

		if (pathWithState.size() > 1)
		{
			list<PathTrellis> pathsLowestMetrics = findPathsLowestMetric(pathWithState);

			if (pathsLowestMetrics.size() == 1 || pathWithState.size() >= 3)
				deletePathsFromTrellis(pathsLowestMetrics);
		}
	}
}

void PathDiscarder::deletePaths()
{
	for (;;)
	{
		trellisIn->nb_read(trellisPaths2Discard);
		PathsElimination();
		trellisOut->nb_write(trellisPaths2Discard);
		if (trellisPaths2Discard.size() != 0 && trellisPaths2Discard.front().nodes.size() == 8)
		{
			out2Traceback.write(true);
			break;
		}
		wait();
	}
	
}