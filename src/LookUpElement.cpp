#include "LookUpElement.h"

// Definition for the friend method.
std::ostream& operator << (std::ostream& os, const LookUpElement& I)
{
	return os;
}

void sc_trace(sc_trace_file*& tf, const LookUpElement& trans, std::string nm)
{
	sc_trace(tf, trans.input_bit, nm + ".input_bit");
	sc_trace(tf, trans.input_state, nm + ".input_state");
	sc_trace(tf, trans.output_state, nm + ".output_state");
	sc_trace(tf, trans.output_bit, nm + ".output_bit");
}