#include "Viterbi.h"
#ifdef Viterbi
list<LookUpElement> Viterbi::checkNextState(uint8_t currentState)
{
	list<LookUpElement> nextStates;

	for (list<LookUpElement>::iterator it = lookUpTable->lookUpTable.begin();
		it != lookUpTable->lookUpTable.end(); ++it)
	{
		if (currentState == it->input_state)
		{
			LookUpElement lk = { it->input_bit, it->input_state, it->output_state, it->output_bit };
			nextStates.push_front(lk);
		}
	}
	return nextStates;
}

uint8_t Viterbi::calculateMetric(LookUpElement lkUpEl)
{
	uint8_t metric = 0;

	((lkUpEl.output_bit & 0x01) == (currentInput & 0x01)) ? metric += 1 : false;
	((lkUpEl.output_bit & 0x02) == (currentInput & 0x02)) ? metric += 1 : false;

	return metric;
}

void Viterbi::createTrellis()
{
	list<Path> _paths = pathsTrellis;

	if (pathsTrellis.size() == 0)
	{
		list<LookUpElement> nextStates = checkNextState(currentState);

		for (list<LookUpElement>::iterator nextState = nextStates.begin();
			nextState != nextStates.end(); nextState++)
		{
			Node nextNode = { nextState->output_state, currentState,
				calculateMetric(*nextState), nextState->output_bit, nextState->input_bit };
			Path _path;
			_path.nodes.push_front(currentNode);
			_path.nodes.push_front(nextNode);
			pathsTrellis.push_front(_path);
			auto var = nextStates.end();

		}
	}
	else
	{
		for (list<Path>::iterator path = pathsTrellis.begin();
			path != pathsTrellis.end(); ++path)
		{
			Node frontNodePath = path->nodes.front();

			list<LookUpElement> nextStates = checkNextState(frontNodePath.state);

			bool firstPathFlag = false;
			Path _path;

			for (list<LookUpElement>::iterator nextState = nextStates.begin();
				nextState != nextStates.end(); nextState++)
			{

				Node nextNode = { nextState->output_state, frontNodePath.state,
					calculateMetric(*nextState) + frontNodePath.metric, nextState->output_bit, nextState->input_bit };

				if (firstPathFlag == false)
				{
					_path.nodes = path->nodes;
					path->nodes.push_front(nextNode);
					firstPathFlag = true;
				}
				else
				{
					_path.nodes.push_front(nextNode);
					pathsTrellis.push_front(_path);
					firstPathFlag = false;
				}
			}
		}
	}
}


list<PathTrellis> Viterbi::findPathsWithSameState(uint8_t state)
{
	list<PathTrellis> pathsRepeated;

	for (list<Path>::iterator path = pathsTrellis.begin();
		path != pathsTrellis.end(); ++path)
	{
		if (state == path->nodes.front().state)
		{
			PathTrellis pathTrellis = { *path, path };
			pathsRepeated.push_front(pathTrellis);
		}
	}

	return pathsRepeated;
}

uint8_t Viterbi::findLowestMetric(list<PathTrellis> paths)
{
	list<uint8_t> metricList;

	for (list<PathTrellis>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		metricList.push_front(path->path.nodes.front().metric);
	}

	auto ite = min_element(metricList.begin(), metricList.end());

	uint8_t minMetric = (uint8_t)* ite;

	return minMetric;
}

list<PathTrellis> Viterbi::findPathsLowestMetric(list<PathTrellis> paths)
{
	list<PathTrellis> pathsLowestMetric;

	uint8_t lowestMetric = 0;

	lowestMetric = findLowestMetric(paths);

	for (list<PathTrellis>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		if (path->path.nodes.front().metric == lowestMetric)
		{
			pathsLowestMetric.push_front(*path);
		}
	}
	return pathsLowestMetric;
}

uint8_t Viterbi::findHighestMetric(list<Path> paths)
{
	list<uint8_t> metricList;

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		metricList.push_front(path->nodes.front().metric);
	}

	auto ite = max_element(metricList.begin(), metricList.end());

	uint8_t maxMetric = (uint8_t)* ite;

	return maxMetric;
}


Path Viterbi::findPathWithHighestMetric(list<Path> paths)
{
	Path pathHighestMetric;

	uint8_t highestMetric = 0;

	highestMetric = findHighestMetric(paths);

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		if (path->nodes.front().metric == highestMetric)
			pathHighestMetric = *path;
	}

	return pathHighestMetric;
}

void Viterbi::deletePathsFromTrellis(list<PathTrellis> paths2Delete)
{
	for (list<PathTrellis>::iterator path = paths2Delete.begin();
		path != paths2Delete.end(); ++path)
	{
		pathsTrellis.erase(path->pathIt);
	}
}

void Viterbi::deletePaths()
{
	for (uint8_t i = 0; i < 8; i++)
	{
		list<PathTrellis> pathWithState = findPathsWithSameState(states[i]);

		if (pathWithState.size() > 1)
		{
			list<PathTrellis> pathsLowestMetrics = findPathsLowestMetric(pathWithState);

			if (pathsLowestMetrics.size() == 1 || pathWithState.size() >= 3)
				deletePathsFromTrellis(pathsLowestMetrics);
		}
	}
}

void Viterbi::do_something()
{
	//list<LookUpElement> nextStates = checkNextState(0b000);
		//currentInput = inB.read() << 1 | inA.read();
	currentInput = input.read();
	createTrellis();
	deletePaths();
	Path pathSolution = findPathWithHighestMetric(pathsTrellis);
	//cout << " inA:  " << inA.read() << endl;
	//cout << " inB:  " << inB.read() << endl;	
	cout << " currentInput:  " << currentInput << endl;
}

#endif