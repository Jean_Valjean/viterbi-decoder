// ViterbiEncoderDecoder.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "systemc.h"
#include "InputShifter.h"
#include "Viterbi.h"
#include "bus_if_path_trellis.h"
#include "bus_trellis.h"
#include "Trellis.h"
#include "PathDiscarding.h"
#include "PathTraceBack.h"
#include "Monitor.h"


int sc_main(int argc, char** argv)
{
	//Clocks
	sc_clock InputClock("InputClock", 50, SC_NS);
	sc_clock PathDiscarderClock("DiscardClokc", 10, SC_NS);

	//Signals
	sc_signal<uint8_t> trellisInput{" Trellis Input "};
	sc_signal<bool> trellisClk{ "Trellis Clock " }, 
		out2Discarder{"trellis to discarder"};
	sc_signal<bool> triggDiscarder2Traceback;
	sc_signal<uint8_t> decoderOutput;
	sc_signal<bool> monitorTrigger;

	//Data buses
	bus_trellis trellisPathsDataBus("Trellis Data Bus");	

	//Modules
	InputShifter input("Input");
	Trellis trellisGenerator("Trellis Generator");
	PathDiscarder pathDiscarder("Path Eliminator");
	Traceback pathTraceback("ResultFinder");
	Monitor monitor("Monitor");

	//Events
	sc_event eventPathDiscardEvent;

	input.output2Trellis(trellisInput);
	input.trellisClk(trellisClk);
	input.clk(InputClock);

	trellisGenerator.input(trellisInput);
	trellisGenerator.clk(trellisClk);
	trellisGenerator.trellisOut(trellisPathsDataBus);
	trellisGenerator.trellisIn(trellisPathsDataBus);

	pathDiscarder.clk(PathDiscarderClock);
	pathDiscarder.trellisIn(trellisPathsDataBus);
	pathDiscarder.trellisOut(trellisPathsDataBus);
	pathDiscarder.out2Traceback(triggDiscarder2Traceback);

	pathTraceback.trigger(triggDiscarder2Traceback);
	pathTraceback.pathsIn(trellisPathsDataBus);
	pathTraceback.decoderOutput(decoderOutput);
	pathTraceback.triggerMonitor(monitorTrigger);

	monitor.trigg(monitorTrigger);
	monitor.decoderOut(decoderOutput);

	sc_start(1000, SC_NS);
	
	return 0;
}
