#include "Trellis.h"


list<LookUpElement> Trellis::checkNextState(uint8_t currentState)
{
	list<LookUpElement> nextStates;

	for (list<LookUpElement>::iterator it = lookUpTable->lookUpTable.begin();
		it != lookUpTable->lookUpTable.end(); ++it)
	{
		if (currentState == it->input_state)
		{
			LookUpElement lk = { it->input_bit, it->input_state, it->output_state, it->output_bit };
			nextStates.push_front(lk);
		}
	}
	return nextStates;
}

uint8_t Trellis::calculateMetric(LookUpElement lkUpEl)
{
	uint8_t metric = 0;

	((lkUpEl.output_bit & 0x01) == (currentInput & 0x01)) ? metric += 1 : false;
	((lkUpEl.output_bit & 0x02) == (currentInput & 0x02)) ? metric += 1 : false;

	return metric;
}


void Trellis::createTrellis()
{
	currentInput = input.read();

	trellisIn->nb_read(pathsTrellis);

	if (pathsTrellis.size() == 0)
	{
		list<LookUpElement> nextStates = checkNextState(currentState);

		for (list<LookUpElement>::iterator nextState = nextStates.begin();
			nextState != nextStates.end(); nextState++)
		{
			Node nextNode = { nextState->output_state, currentState,
				calculateMetric(*nextState), nextState->output_bit, nextState->input_bit };
			Path _path;
			_path.nodes.push_front(currentNode);
			_path.nodes.push_front(nextNode);
			pathsTrellis.push_front(_path);
		}
	}
	else
	{
		for (list<Path>::iterator path = pathsTrellis.begin();
			path != pathsTrellis.end(); ++path)
		{
			Node frontNodePath = path->nodes.front();

			list<LookUpElement> nextStates = checkNextState(frontNodePath.state);

			bool firstPathFlag = false;
			Path _path;

			for (list<LookUpElement>::iterator nextState = nextStates.begin();
				nextState != nextStates.end(); nextState++)
			{
				Node nextNode = { nextState->output_state, frontNodePath.state,
					calculateMetric(*nextState) + frontNodePath.metric, nextState->output_bit, nextState->input_bit };

				if (firstPathFlag == false)
				{
					_path.nodes = path->nodes;
					path->nodes.push_front(nextNode);
					Path newPath;
					newPath.nodes = path->nodes;
					firstPathFlag = true;
				}
				else
				{
					_path.nodes.push_front(nextNode);
					pathsTrellis.push_front(_path);
					firstPathFlag = false;
				}
			}
		}

	}
	trellisOut->nb_write(pathsTrellis);

}