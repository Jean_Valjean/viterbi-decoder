#include "PathTraceBack.h"

uint8_t Traceback::findHighestMetric(list<Path> paths)
{
	list<uint8_t> metricList;

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		metricList.push_front(path->nodes.front().metric);
	}

	auto ite = max_element(metricList.begin(), metricList.end());

	uint8_t maxMetric = (uint8_t)* ite;

	return maxMetric;
}

Path Traceback::findPathWithHighestMetric(list<Path> paths)
{
	Path pathHighestMetric;

	uint8_t highestMetric = 0;

	highestMetric = findHighestMetric(paths);

	for (list<Path>::iterator path = paths.begin();
		path != paths.end(); ++path)
	{
		if (path->nodes.front().metric == highestMetric)
			pathHighestMetric = *path;
	}

	return pathHighestMetric;
}

void Traceback::findPath()
{
	list<Path> pathList;
	pathsIn->nb_read(pathList);
	Path winnerPath = findPathWithHighestMetric(pathList);

	uint8_t count = 0;
	uint8_t tempRes = 0;

	for (list<Node>::iterator node = winnerPath.nodes.begin();
		node != winnerPath.nodes.end(); ++node)
	{
		tempRes = (node->input_bit << count) | tempRes;		
		count++;
	}
	decoderOutput.write(tempRes);
	triggerMonitor.write(true);

}