#include "InputShifter.h"


void InputShifter::bin(unsigned n)
{
	if (n > 1)
		bin(n >> 1);
	cout << (n & 1);
}

void InputShifter::shift_input()
{
	cout << endl;
	cout << endl;
	cout << endl;
	cout << "------------------ Input to the Decoder ------------------" << endl;
	bin(unsigned(data_in));
	cout << endl;

	for (int i = 13; i >= 0; i -= 2)
	{
		uint8_t varTest = data_in[i] << 1 | data_in[i - 1];
		output2Trellis.write(varTest);
		trellisClk.write(!trellisClk.read());
		wait();		
		
		//cout << data_in[i] << endl;
		//cout << data_in[i - 1] << endl;
		
	}
}